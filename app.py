from flask import Flask, request

app = Flask(__name__)
methods = ["GET", "POST", "PATCH", "DELETE"]


@app.route("/api", methods=methods)
def hi():
    print(request.form)
    return request.form


if __name__ == "__main__":
    app.run()